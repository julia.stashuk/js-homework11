let passwordForm = document.querySelector(".password-form");
let password = document.querySelector(".password");
let confirmPassword = document.querySelector(".confirm-password");
let buttonSubmit = document.querySelector(".btn");

let checkPassword = function (e) {
    e.preventDefault();
    if(password.value === ""){
        return document.querySelector(".text-error").innerText = "Введіть пароль";
    } else if(password.value === confirmPassword.value){
        return alert("You are welcome")
    } else {
        document.querySelector(".text-error").innerText = "Потрібно ввести однакові значення";
    }
}

buttonSubmit.addEventListener("click", checkPassword);

passwordForm.addEventListener("click", (e) =>{
    if(e.target.classList.contains("fa-eye")){
        e.target.classList.replace("fa-eye", "fa-eye-slash");
        e.target.previousElementSibling.setAttribute("type", "text")
    } else {
        e.target.classList.replace("fa-eye-slash", "fa-eye");
        e.target.previousElementSibling.setAttribute("type", "password")
    }
})
